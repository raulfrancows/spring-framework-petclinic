# Spring PetClinic Sample Application

One of the best parts about working on the Spring Petclinic application is that we have the opportunity to work in direct contact with many Open Source projects. We found some bugs/suggested improvements on various topics such as Spring, Spring Data, Bean Validation and even Eclipse! In many cases, they've been fixed/implemented in just a few days.

# Descripcion

Practico sistema de administración de una veterinaria

url jboss: http://134.122.117.179:9990
url web: http://134.122.117.179:8080/petclinic/
url jenkins: http://143.198.20.233:8080
url sonarcloud: http://143.198.20.233:9000
url artifactory: http://143.198.20.233:8082
